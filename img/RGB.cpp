#include "pch.h"
#include "RGB.h"

RGB::RGB() {
	R = B = G = -1;
}

RGB::RGB(unsigned char r, unsigned char g, unsigned char b) {
	R = r; G = g; B = b;
}

RGB RGB::AsGrayscale() const {
	unsigned char mean = (R + G + B) / 3;
	return RGB(mean, mean, mean);
}

unsigned long RGB::MakeDword() const {
	return (R) | (G << 8) | (B << 16);
}