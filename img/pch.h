#pragma once

#include <Windows.h>
#include <windowsx.h>

#include <atlbase.h>
#include <atlapp.h>
#include <atlwin.h>
#include <atltypes.h>

#include <thread>
#include <vector>
#include <string>
#include <chrono>
#include <random>

// GDI macro
#undef RGB

// Stupid macro
#undef ZeroMemory
template <typename t>
void ZeroMemory(t& mem) {
	memset(&mem, 0, sizeof(t));
}