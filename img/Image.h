#include "rgb.h"

class CImage {
public:
	int rows;
	int cols;

	bool regenerating;
	std::thread regen_thread;

	std::vector<RGB> rgb;

	CImage();
	~CImage();

	RGB& px(int row, int col);

	void Resize(int width, int height);

	void Regenerate();
};