#include "pch.h"
#include "resource.h"

#include "Image.h"

class CMainWindow 
	: public CWindowImpl <CMainWindow>
{
public:
	BEGIN_MSG_MAP(CMainWindow)
		COMMAND_ID_HANDLER(ID_MENU_REGEN, onMenuRegen)
		COMMAND_ID_HANDLER(ID_MENU_SETTINGS, onMenuSettings)
		COMMAND_ID_HANDLER(ID_MENU_QUIT, onMenuQuit)

		COMMAND_RANGE_CODE_HANDLER(ID_SETTINGS_256X256, ID_SETTINGS_1280X720, BN_CLICKED, onMenuSettings);

		MESSAGE_HANDLER(WM_CREATE, onCreate);
		MESSAGE_HANDLER(WM_SIZE, onResize);
		MESSAGE_HANDLER(WM_KEYDOWN, onKeyDown)
		MESSAGE_HANDLER(WM_DESTROY, onDestroy)
		MESSAGE_HANDLER(WM_PAINT, onPaint)
	END_MSG_MAP()

	CImage img;

	LRESULT onMenuRegen(WORD code, WORD id, HWND, BOOL&) {
		img.Regenerate();
		InvalidateRect(nullptr);
		return 0;
	}

	LRESULT onMenuSettings(WORD code, WORD id, HWND, BOOL&) {
		switch (id) {
		case ID_SETTINGS_256X256: ResizeClient(256, 256); break;
		case ID_SETTINGS_512X512: ResizeClient(512, 512); break;
		case ID_SETTINGS_640X480: ResizeClient(640, 480); break;
		case ID_SETTINGS_800X600: ResizeClient(800, 600); break;
		case ID_SETTINGS_1280X720: ResizeClient(1280, 720); break;
		default: break;
		}
		return 0;
	}

	LRESULT onMenuQuit(WORD, WORD, HWND, BOOL&) {
		DestroyWindow();
		return 0;
	}

	LRESULT onCreate(UINT, WPARAM, LPARAM, BOOL&) {
		return 0;
	}

	LRESULT onKeyDown(UINT, WPARAM wKey, LPARAM, BOOL&) {
		if (wKey == VK_ESCAPE) {
			DestroyWindow();
		}
		else if (wKey == VK_RETURN) {
			img.Regenerate();
			InvalidateRect(nullptr);
		}
		return 0;
	}

	LRESULT onDestroy(UINT, WPARAM, LPARAM, BOOL&) {
		PostQuitMessage(0);
		return 0;
	}

	LRESULT onPaint(UINT, WPARAM, LPARAM, BOOL&) {
		PAINTSTRUCT ps;
		CDC hDC = BeginPaint(&ps);

		for (int row = 0; row < img.rows; row++) {
			for (int col = 0; col < img.cols; col++) {
				hDC.SetPixel(col, row, img.px(row, col));
			}
		}

		EndPaint(&ps);
		return 0;
	}

	LRESULT onResize(UINT, WPARAM, LPARAM lParam, BOOL&) {
		CRect rc;
		GetClientRect(&rc);
		
		img.Resize(rc.Width(), rc.Height());

		return 0;
	}
};

auto WINAPI WinMain(HINSTANCE, HINSTANCE, char*, int) -> int {
	const DWORD dwStyle = WS_OVERLAPPEDWINDOW ^ WS_MAXIMIZEBOX ^ WS_THICKFRAME | WS_VISIBLE;
	const DWORD dwExStyle = WS_EX_APPWINDOW;

	CMenu menu;
	menu.LoadMenuW(MAKEINTRESOURCE(IDR_MENU1));

	CMainWindow wnd;
	wnd.Create(nullptr, CRect(0, 0, 256, 256), L"img", dwStyle, dwExStyle);
	wnd.SetMenu(menu);
	wnd.ResizeClient(512, 512);

	CMessageLoop loop;
	return loop.Run();
}