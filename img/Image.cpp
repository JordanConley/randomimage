#include "pch.h"
#include "image.h"
#include "Stopwatch.h"

CImage::CImage() {
	rows = 1;
	cols = 1;
	regenerating = false;

	rgb.resize(rows * cols);
}

CImage::~CImage() {
	if (regen_thread.joinable()) {
		regen_thread.join();
	}
}

RGB& CImage::px(int row, int col) {
	if (row >= rows) {
		throw std::exception("Bad row");
	}
	
	if (col >= cols) {
		throw std::exception("Bad col");
	}

	return rgb[row * cols + col];
}

void CImage::Resize(int width, int height) {
	rows = height;
	cols = width;

	rgb.resize(width * height);
}

void InternalRegen(CImage* th) {
	CStopwatch sw;

	std::random_device rd;
	std::mt19937 engine{ rd() };
	std::normal_distribution<> dist{ 255 / 2, 128};

	th->regenerating = true;
	sw.start();
	for (RGB& col : th->rgb) {
		col = RGB(dist(engine), dist(engine), dist(engine));
	}
	double d = sw.end();
	OutputDebugStringA(std::string("Created image in " + std::to_string(d) + "s\n").c_str());

	th->regenerating = false;
}

void CImage::Regenerate() {
	if (regen_thread.joinable())
		regen_thread.join();

	if (regenerating == false)
		regen_thread = std::thread(InternalRegen, this);
}