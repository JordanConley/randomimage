#include "pch.h"
#include "Stopwatch.h"

using namespace std;
using namespace std::chrono;

void CStopwatch::start() {
	begin = high_resolution_clock::now();
}

double CStopwatch::end() {
	auto end = high_resolution_clock::now();

	auto d = duration_cast<std::chrono::nanoseconds>(end - begin).count();

	return static_cast<double>(d)* pow(10, -9);
}