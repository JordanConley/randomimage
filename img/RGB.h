class RGB {
public:
	unsigned char R, G, B;

	RGB();

	RGB(unsigned char r, unsigned char g, unsigned char b);

	RGB AsGrayscale() const;

	unsigned long MakeDword() const;
	
	operator unsigned long() const {
		return MakeDword();
	}
};
