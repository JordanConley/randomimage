#pragma once


class CStopwatch
{
private:
	std::chrono::high_resolution_clock::time_point begin;

public:
	void start();
	double end();
};

