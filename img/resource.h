//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by img.rc
//
#define IDR_MENU1                       101
#define ID_MENU_REGEN                   40001
#define ID_MENU_SETTINGS                40002
#define ID_MENU_QUIT                    40003
#define ID_SETTINGS_256X256             40004
#define ID_SETTINGS_512X512             40005
#define ID_SETTINGS_640X480             40006
#define ID_SETTINGS_800X600             40007
#define ID_SETTINGS_1280X720            40008
#define ID_SETTINGS_SETSEED             40009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40010
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
